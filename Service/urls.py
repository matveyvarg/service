"""Service URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from base import views
urlpatterns = [
    url(r'^admin/', admin.site.urls, name='superuser_admin'),
    url(r'^', include('base.urls')),
    url(r'^$',views.get_form, name='index'),
    url(r'^admin_panel$', views.get_admin_panel, name='index_admin'),
    url(r'^admin_panel/responses', views.responses, name='responses'),
    # url(r'^processing/', include('permabots.urls_processing', namespace="permabots"))
    # url(r'^webhook', views.webhooks, name='webhook')
]
