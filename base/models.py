# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class City(models.Model):
    name = models.CharField(max_length=255, default='')


class UserMeta(models.Model):

    phone = models.CharField(max_length=13)
    auth_field = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return u"{} |{}".format(self.phone, self.auth_field.username)

    class Meta:
        verbose_name = u'Клиент'
        verbose_name_plural = u'Клиенты'


class Item(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        abstract = True

    def __unicode__(self):
        return u"{}".format(self.name)

    def __str__(self):
        return self.__unicode__()


class ItemType(Item):

    class Meta:
        verbose_name = 'Тип техники'
        verbose_name_plural = 'Типы техники'


class ItemBrand(Item):
    item_type = models.ForeignKey(ItemType, related_name='brands', on_delete=models.CASCADE)

    def __unicode__(self):
        return u"{} {}".format(self.item_type.name, self.name)

    class Meta:
        verbose_name = u'Марка'
        verbose_name_plural = u'Марки'


class OrderItem(Item):
    item_type = models.ForeignKey(ItemType, related_name='models', on_delete=models.CASCADE)

    def __unicode__(self):
        return u"{} {} {}".format(self.item_type, self.name)

    class Meta:
        verbose_name = u'Модель'
        verbose_name_plural = u'Модели'


class Area(models.Model):
    name = models.CharField(max_length=33)

    def __unicode__(self):
        return u"{}".format(self.name)

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = u'Район'
        verbose_name_plural = u'Районы'


class Streets(models.Model):
    area = models.ForeignKey(Area, related_name='streets', null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=64)

    def __unicode__(self):
        return u"{}".format(self.name)

    class Meta:
        verbose_name = u'Улица'
        verbose_name_plural = u'Улицы'


class Address(models.Model):
    city = models.ForeignKey(City, related_name='addresses', null=True, blank=True, on_delete=models.CASCADE)
    street = models.ForeignKey(Streets, related_name='addresses', on_delete=models.SET_NULL, null=True)
    house = models.CharField(max_length=128, blank=True, default='')
    room = models.CharField(max_length=12, blank=True, default='')

    def __unicode__(self):
        return u"{0} ул.{1} дом.{2} кв.{3}".format(self.city, self.street.name, self.house, self.room)


class Service(models.Model):

    name = models.CharField(max_length=256)
    address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL)
    phone = models.CharField(max_length=13)
    auth_field = models.OneToOneField(User, on_delete=models.CASCADE)
    limit = models.PositiveSmallIntegerField()
    categories = models.ManyToManyField(ItemType, related_name='services')
    area = models.ForeignKey(Area, related_name='services', null=True, blank=True, on_delete=models.SET_NULL)

    def __str(self):
        return self.__unicode__()

    def __unicode__(self):
        return u"{0}".format(self.name)

    class Meta:
        verbose_name = u'Сервисный центр'
        verbose_name_plural = u'Сервисные центры'

    @property
    def get_namefield(self):
        if self.phone:
            return self.phone
        else:
            return self.auth_field.email

    def is_service(self):
        return True

class Rating(models.Model):

    vote_for = models.ForeignKey(Service, related_name='rating', on_delete=models.CASCADE)
    vote_rate = models.PositiveSmallIntegerField()


class Courier(models.Model):
    auth_field = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=13)
    passport_number = models.CharField(max_length=11, blank=True)
    area = models.ForeignKey(Area, related_name='couriers', null=True, blank=True, on_delete=models.SET_NULL)
    categories = models.ManyToManyField(ItemType, related_name='couriers')
    is_just_corier = models.BooleanField(default=True)

    @property
    def get_namefield(self):
        if self.phone:
            return self.phone
        else:
            return self.auth_field.email

    @property
    def is_busy(self):
        return self.orders.filter(is_closed=False).exists()

    def __str__(self):
        return "{} {} |{}".format(self.auth_field.last_name, self.auth_field.first_name, self.auth_field.username)

    def is_service(self):
        return False

    class Meta:
        verbose_name_plural = u'Курьеры'
        verbose_name = u'Курьер'


class Contact(models.Model):
    name = models.CharField(max_length=64, blank=True)
    phone = models.CharField(max_length=16)

    class Meta:
        verbose_name_plural = 'Контакты'
        verbose_name = 'Контакт'


class Order(models.Model):

    item = models.ForeignKey(OrderItem, related_name='orders', null=True, on_delete=models.SET_NULL)
    case = models.TextField()
    user = models.ForeignKey(UserMeta, null=True, related_name='orders', on_delete=models.CASCADE)
    address = models.ForeignKey(Address, null=True, on_delete=models.SET_NULL)
    answered_by = models.ManyToManyField(Service, related_name='answered_orders', null=True, blank=True)
    courier = models.ForeignKey(Courier, related_name='orders', null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    is_closed = models.BooleanField(default=False)
    contact = models.ForeignKey(Contact, related_name='orders', null=True, blank=True, on_delete=models.SET_NULL)
    service = models.ForeignKey(Service, related_name='services', on_delete=models.CASCADE, null=True, blank=True)

    def __unicode__(self):
        return u"{0} - {1}".format(self.item.__str__(), self.case)


class Risk(models.Model):
    service = models.ManyToManyField(Service)
    order = models.ForeignKey(Order, related_name='risks', on_delete=models.CASCADE)
    _risk = models.PositiveSmallIntegerField(default=0)

    def get_risk(self):
        return self._risk / 100

    def set_risk(self, value):
        self._risk = value

    value = property(get_risk, set_risk)



