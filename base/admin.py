# -*- coding:utf-8 -*-

from django.contrib import admin
from base.models import *
# Register your models here.
from django.contrib.auth.models import Group, User


class ItemAdmin(admin.ModelAdmin):
    pass


class ServiceAdmin(admin.ModelAdmin):
    pass


class UserAdmin(admin.ModelAdmin):
    pass

class OrderAdmin(admin.ModelAdmin):
    pass


@admin.register(Courier)
class CourierAdmin(admin.ModelAdmin):
    pass


@admin.register(Address)
class AdressAdmin(admin.ModelAdmin):
    pass


@admin.register(Area)
class AreaAdmin(admin.ModelAdmin):
    pass


@admin.register(Streets)
class StreetsAdmin(admin.ModelAdmin):
    pass


@admin.register(ItemType)
class ItemTypeAdmin(admin.ModelAdmin):
    pass


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    pass


@admin.register(Contact)
class ContactsAdmin(admin.ModelAdmin):
    pass


admin.site.register(UserMeta, UserAdmin)
admin.site.register(Service, ServiceAdmin)
admin.site.register(Order, OrderAdmin)

admin.site.unregister(Group)
# admin.site.unregister(User)
