# -*- coding: utf-8 -*-

from django.shortcuts import render, get_object_or_404, redirect
from django.http.response import JsonResponse
from django.db.models import Q
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages

from rest_framework.renderers import TemplateHTMLRenderer, JSONRenderer
from rest_framework.response import Response
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.exceptions import ValidationError

from base.models import *
from base.serializers import *

from re import sub
# Create your views here.


class OrderCreate(generics.ListCreateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

    def get_queryset(self):
        if self.request.user:
            try:
                user_obj = Courier.objects.get(auth_field=self.request.user)
            except BaseException:
                try:
                    user_obj = Service.objects.get(auth_field=self.request.user)
                except BaseException:
                    return self.queryset.order_by('created_at')

            return self.queryset.filter(Q(address__street__area=user_obj.area) | Q(address__street__area=None) | Q(address__street__area__isnull=True),
                                           item__item_type__in=user_obj.categories.all(),is_closed=False)
        return self.queryset


    def create(self, request, *args, **kwargs):

        serializer = self.get_serializer(data=request.data)

        serializer.is_valid(raise_exception=True)

        error = ''

        if request.POST.get('item_type') == u'Тип':
            error = error + ' Укажите тип.'

        if not request.POST.get('phone'):
            error += 'Укажите телефон'

        if error:
            messages.error(self.request._request, error)
            return redirect('index')

        try:
            self.perform_create(serializer)
            messages.success(self.request._request, 'Заявка успешно создана')

        except ValidationError as e:
            messages.error(self.request._request, 'Что-то пошло не так')

        return redirect('index')

    def perform_create(self, serializer):
        (street, house) = (self.request.POST.get('street'),
                           self.request.POST.get('house'),)

        if not street or not house:
            print('Одно из полей адреса пустое')
            raise ValidationError(u'Одно из полей адреса пустое')

        (street, street_is_created) = Streets.objects.get_or_create(name=street)
        try:
            (address, address_is_created) = Address.objects.get_or_create(
                street=street,
                house=self.request.POST['house'],
            )
        except BaseException as e:
            print(u'Что-то пошло не так при создании адреса', str(e))
            raise ValidationError(u'Что-то пошло не так при создании адреса')

        try:
            (item_type, created) = ItemType.objects.get_or_create(name=self.request.POST.get('item_type'))
            (item_model, created) = OrderItem.objects.get_or_create(name=self.request.POST.get('item_model'),
                                                                   item_type=item_type)
        except BaseException as e:
            print(u'Что-то пошло не так при создании сущности', str(e))
            raise ValidationError(u'Что-то пошло не так при создании сущности')

        if self.request.user and self.request.user.is_authenticated:
            user = UserMeta.objects.get_or_create(auth_field=self.request.user)[0]
        else:
            user = None

        (contact, created) = Contact.objects.get_or_create(phone=self.request.POST.get('phone'))
        if self.request.POST.get('name'):
            contact.name = self.request.POST.get('name')
            contact.save()

        serializer.save(user=user, address=address, item=item_model)


class AnswerOrder(APIView):

    def post(self, request):
        if not request.user is None and request.user.is_authenticated:
            try:
                identity = Service.objects.get(auth_field=request.user)
            except Service.DoesNotExist:
                try:
                    identity = Courier.objects.get(auth_field=request.user)
                except Courier.DoesNotExist:
                    identity = None

            if identity:
                order = Order.objects.get(pk=request.POST['id'])
                if isinstance(identity, Service):

                    if identity not in order.answered_by.all():
                        order.answered_by.add(identity)
                        order.save()
                        serializer = OrderSerializer(order)
                        return Response(serializer.data)

                    else:
                        return Response({"error": u"Вы уже ответили на эту заявку"})
                elif isinstance(identity, Courier):
                    if order.courier:
                        return Response({"error":"У заявки уже есть курьер"})
                    order.courier = identity
                    order.save()
                    serializer = OrderSerializer(order)
                    return Response(serializer.data)
            else:
                return Response({"error": u"Вы не найдены в системе, пожалуйста обратитесь в поддержку"})

        else:
            return Response({"error": u"Пожалуйста, войдите в систему"})


class GetResponses(generics.ListAPIView):
    serializer_class = OrderSerializer

    def get_queryset(self):
        if self.request.user:
            try:
                user_obj = Service.objects.get(auth_field=self.request.user)
            except BaseException:
                try:
                    user_obj = Courier.objects.get(auth_field=self.request.user)
                except BaseException:
                    return Order.objects.filter()

            if isinstance(user_obj, Courier):
                return Order.objects.filter(courier=user_obj)

            elif isinstance(user_obj, Service):
                return Order.objects.filter(answered_by__in=user_obj)

        return Order.objects.filter()


class GetCurrentOrder(generics.RetrieveAPIView):
    serializer_class = OrderSerializer
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]

    def get_object(self):
        try:
            courier = Courier.objects.get(auth_field=self.request.user)
            return Order.objects.filter(courier=courier, is_closed=False).order_by('created_at').last()
        except BaseException:
            try:
                service = Service.objects.get(auth_field=self.request.user)
                return Order.objects.filter(service=courier, is_closed=False).order_by('created_at').last()
            except BaseException:
                return None

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance:

           return Response({}, template_name='base/current_order.html', content_type='application/json')

        return Response({'order': instance}, template_name='base/current_order.html')


class ItemsView(generics.ListAPIView):
    serializer_class = AllItemSerializer
    queryset = ItemType.objects.all()



def close_order(request):
    if request.POST and 'id' in request.POST:
        try:
            order = Order.objects.get(pk=request.POST.get('id'))
            order.is_closed = True
            order.save()
            return JsonResponse({"success": 1})

        except BaseException:
            pass

    return JsonResponse({"error": u"Что-то пошло не так при закрытии заказа, "
                                     u"пожалуйста, попробуйте снова или обратитесь в службу поддержки"})


def responses(request):
    return render(request, 'base/responses.html')


def get_form(requset):
    return render(requset, 'landing/index.html')


def get_admin_panel(request):
    if request.user:
        if request.user.is_authenticated:

            try:
                courier = Courier.objects.get(auth_field=request.user)
            except BaseException:
		try:
		    courier = Service.objects.get(auth_field=request.user)
		except BaseException:

	            return render(request, 'base/login.html')

            return render(request, 'base/starter.html', {'courier':courier})

    context = {}
    if request.session.get('login_error'):
        context['error'] = request.session.get('login_error')
        del request.session['login_error']

    return render(request, 'base/login.html', context)


def admin_login(request):
    username = request.POST.get('username')
    password = request.POST.get('pass')

    if username and password:
        user = authenticate(username=username,password=password)
	
        if user:
            login(request, user)
            return redirect('index_admin')

        else:
            request.session['login_error'] = 'Пользователь не найден'
            return redirect('index_admin')

    request.session['login_error'] = 'Ошибка в передаче данных, пожалуйста обратитесь в поддержку'
    return redirect('index_admin')


def user_logout(request):
    return_page = request.user.is_superuser and '/admin/' or 'index_admin'
    logout(request)
    return redirect(return_page)
