from rest_framework import serializers
from base.models import *

def filter_answered_by(answered_by):
    """
    Filter answered orders by 3 criteria (price, rating and risk)
    :param answered_by: 
    :return: 
    """
    # for answer in answered_by:
    if len(answered_by) > 0:
        print(dict(answered_by[0]))
    return answered_by


class ItemSerializer(serializers.Serializer):

    item_type = serializers.StringRelatedField(source='item_brand.item_type')
    item_brand = serializers.StringRelatedField(source='item_brand.name')
    item_model = serializers.CharField(source='name')


class AddressSerializer(serializers.ModelSerializer):

    class Meta:
        fields = '__all__'
        model = Address

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        exclude = ('auth_field', )
        depth = 2


class ContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        exclude = ('id',)


class OrderSerializer(serializers.ModelSerializer):

    item = ItemSerializer(read_only=True)
    answered_by = ServiceSerializer(read_only=True, many=True)
    contact = ContactSerializer(read_only=True)

    class Meta:
        fields = '__all__'
        model = Order


class AllItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemType
        fields = '__all__'

