$(document).ready(function(){

	// Fix Header
	// Options
	var options = {
	  offset: 500
	}

	// Create a new instance of Headhesive.js and pass in some options
	var headerFix = new Headhesive('header', options);
	// end Fix Header
	

	// Particles
		particlesJS.load('particles', 'particlesjs-config.json', function() {});
		particlesJS.load('particles_2', 'particlesjs-config.json', function() {});

	// end Particles

	$.ajax({
            url: 'get_items/',
            data: {require_for: 'item_type'},
            method: 'GET',
            success: function (res) {
            	localStorage.setItem('types', JSON.stringify(res))
                res.forEach(function (item) {

                    $('form select[name="item_type"]').append('<option value="' + item.name+ '">' + item.name + '</option>');


                });
				$("select").niceSelect();

            }
	});

	var PartnerSlider = new Swiper ('.partners__slider', {
    loop: true,
    slidesPerView: 4,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
     breakpoints: {
        1920: {
          slidesPerView: 4
        },
        1170: {
          slidesPerView: 3
        },
        960: {
          slidesPerView: 2
        },
        600: {
          slidesPerView: 1
        }
      }

  });


	$("a.Toogle_menu").click(function(){
		$("div.m__menu__").toggleClass("m__menu__active");
		return false;
	});

	$("ul.m__menu__nav a.close").click(function(){
		$("div.m__menu__").toggleClass("m__menu__active");
		return false;
	});

	$("a.ancor__").click(function(){

	    var target = $(this).attr('href');
	    $('html, body').animate({scrollTop: $(target).offset().top}, 800);
	    
	    return false;
	});

	$("a.ancor__M").click(function(){

		$("div.m__menu__").toggleClass("m__menu__active");

	    var target = $(this).attr('href');
	    $('html, body').animate({scrollTop: $(target).offset().top}, 800);
	    
	    return false;
	});






});