/**
 * Created by matveyvarg on 1/9/18.
 */
(function ($) {
    $(document).ready(function () {

    
        AdminPanel.answerOrder($);

        AdminPanel.getCurrentOrder($, AdminPanel.dataTables);

        AdminPanel.closeOrder($);

        AdminPanel.changeActive($);
    })
})(jQuery);

var AdminPanel = {};

AdminPanel.dataTables = function () {

    var have_current = $('#current_order').find('.current_order-body').length > 0;

    AdminPanel.ordersTable = $('#orders_table').DataTable({
	'scrollX':True,
        ajax: {
            url:'/orders',
            dataSrc:"",
        },
        columns: [
            { "data": "id" },
            { "data": "item.item_type" },
            { "data": "item.item_brand" },
            { "data": "item.item_model" },
            { "data": "case" },
            {
                "render": function (data, type, row, meta) {
                    if (have_current){
                        return '<span class="btn btn-block" disabled>У вас уже есть заявка.</span>';
                    }
                    return '<button class="btn btn-primary">Ответить на заявку</button>';
                }
            }
        ]
    });

    AdminPanel.responesTable = $('#responses_table').DataTable({
	'scrollX': true,
        ajax: {
            url: '/responses',
            dataSrc:"",
        },
        columns: [
            { "data": "id" },
            { "data": "item.item_type" },
            { "data": "item.item_brand" },
            { "data": "item.item_model" },
            { "data": "case" },
        ]
    });
};

AdminPanel.answerOrder = function ($) {
    $('#orders_table tbody').on( 'click', 'button', function () {
        var data = AdminPanel.ordersTable.row( $(this).parents('tr') ).data();
        var csrftoken = Cookies.get('csrftoken');

        $.ajax({
            url:'answer_order/',
            method: 'POST',
            data: {
                id:data['id'],
                csrfmiddlewaretoken: csrftoken,
            },
            success: function () {
                window.location.reload()
            }
        });
    });
};

AdminPanel.getCurrentOrder = function ($, callback) {
    $.ajax({
        url: '/get_current_order',
        method: 'GET',
        success: function (res) {
            if (!res.hasOwnProperty('error')) {
                $('#current_order').html(res).removeClass('box-danger').addClass('box-success')
            }
            callback()
        },
        error: function () {
            callback()
        }
    })
}

AdminPanel.closeOrder = function ($) {

    $('#current_order').on('click','#close_order' ,function () {
        var id = $(this).data('id');
        var csrftoken = Cookies.get('csrftoken');

        $.ajax({
            url: '/close_order',
            method: 'POST',
            data: {
                id:id,
                csrfmiddlewaretoken: csrftoken,

            },
            success: function (r) {
                if(!r.hasOwnProperty('error')){
                    $('#current_order').removeClass('box-success').addClass('box-danger')
                    $('#current_order').html(`<div class="box-header with-border">
                                                <div class="box-title">
                                                    <h3>Текущая заявка:</h3>
                                                </div>
                                            </div>
                                             <div class="box-body">
                                                 У вас пока нет заявок.
                                             </div>`);
                    window.location.reload();

                }
                else {

                    $('#close-order__callout p').html(r.error);
                    $('#close-order__callout').fadeIn()
                }
            },
            error:function (r) {
                $('#close-order__callout p').html(r.error);
                $('#close-order__callout').fadeIn()
            }
        })
    })

};

AdminPanel.changeActive = function ($) {
    var sidebar_menu = $('.sidebar-menu');
    sidebar_menu.find('a').each(function (i) {
        var item = $(this);
        item.parent().removeClass('active');
        if (item.attr('href') == window.location.pathname ) {
            console.log('d')
            item.parent().addClass('active');
        }
    })
};
