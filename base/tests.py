from django.test import TransactionTestCase
from model_mommy import mommy
from .models import ItemType, OrderItem
from .views import OrderCreate
from django.urls import reverse_lazy
from django.test import Client
import string
import random

ALPHABET = string.letters + string.digits

def generate_random_string(max_length):
    length = random.randint(0, max_length-1)
    random_string = ''
    for x in range(0,length):
        random_string += random.choice(ALPHABET)
    return

class OrderTestCase(TransactionTestCase):

    ORDER_URL = reverse_lazy('base:orders')

    def setUp(self):
        self.item_type = mommy.make(ItemType)
        self.order_item = mommy.make(OrderItem, item_type=self.item_type)

    def testFK(self):
        assert self.order_item.item_type == self.item_type

    def testOrder(self):

        response = self.client.post(
            self.ORDER_URL,
            {
                'item_type': random.choice(ItemType.objects.values_list(flat=True)),
                'item_model': generate_random_string(128),
                'street': generate_random_string(64),
                'house': generate_random_string(128),
                'case': generate_random_string(128),
                'phone': generate_random_string(11)
            },
            follow=True,
        )
        assert response.status_code in [200, 301, 302]
        messages = list(response.context['messages'])

        for message in messages:
            self.assertNotEqual(message.tags, 'alert', message)
            self.assertNotEqual(message.tags, 'error', message)





# Create your tests here.
