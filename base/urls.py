from django.conf.urls import url, include

from base import views

app_name = 'base'

urlpatterns = [
    url(r'orders/$', views.OrderCreate.as_view(), name='orders'),
    url(r'get_items/$', views.ItemsView.as_view(), name='get_items'),
    url(r'answer_order/$', views.AnswerOrder.as_view(), name='answer_order'),
    url(r'responses/$', views.GetResponses.as_view(), name='get_responses'),
    url(r'get_current_order', views.GetCurrentOrder.as_view(), name='get_current_view'),
    url(r'close_order', views.close_order, name='close_order'),
    url(r'login/', views.admin_login, name='login'),
    url(r'logout', views.user_logout, name='logout')
]