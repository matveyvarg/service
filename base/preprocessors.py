from django.conf import settings


def global_settings(request):
    # return any necessary values
    return {
        'BRAND_BLOCK': settings.BRAND_BLOCK,
        'NEWS_BLOCK': settings.NEWS_BLOCK,
        'HELP_BLOCK': settings.HELP_BLOCK,
        'REVIEWS_BLOCK': settings.REVIEWS_BLOCK,
        'ABOUT_US_BLOCK': settings.ABOUT_US_BLOCK,
        'HOWITWORKS_BLOCK': settings.HOWITWORKS_BLOCK,
        'STATISTIC_BLOCK': settings.STATISTIC_BLOCK,
        'ADVANTAGES_BLOCK': settings.ADVANTAGES_BLOCK,
    }